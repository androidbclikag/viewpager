package com.example.viewpagerapp.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.viewpagerapp.R
import com.example.viewpagerapp.models.ImageModel

import kotlinx.android.synthetic.main.fragment_gallery.view.*


class ImageGalleryFragment(var image:ImageModel, var position:Int) : Fragment() {
    private lateinit var itemView: View
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        itemView = inflater.inflate(R.layout.fragment_gallery, container, false)
        init()
        return itemView
    }

    private fun init() {
        Glide.with(this).load(image.image)
            .placeholder(R.mipmap.ic_launcher_round).into(itemView.imageView)
        itemView.textView.text = position.toString()
    }

}
