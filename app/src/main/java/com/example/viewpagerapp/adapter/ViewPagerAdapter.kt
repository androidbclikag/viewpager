package com.example.viewpagerapp.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.viewpagerapp.fragment.ImageGalleryFragment
import com.example.viewpagerapp.models.ImageModel


class ViewPagerAdapter(
    fm: FragmentManager,
    behavior: Int,
    private val images: ArrayList<ImageModel>
) : FragmentStatePagerAdapter(fm, behavior) {
    override fun getItem(position: Int): Fragment = ImageGalleryFragment(images[position], position)

    override fun getCount(): Int = images.size
}